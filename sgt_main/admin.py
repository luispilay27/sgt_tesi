from django.contrib import admin
from .models import pais, provincia, ciudad, codigo_telefono, tipo_identificacion

admin.site.register(pais)
admin.site.register(provincia)
admin.site.register(ciudad)
admin.site.register(codigo_telefono)
admin.site.register(tipo_identificacion)