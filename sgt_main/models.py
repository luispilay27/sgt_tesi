from django.db import models
from django.contrib.auth.models import User

class pais(models.Model):
    descripcion = models.CharField(max_length=100)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.descripcion
    
class provincia(models.Model):
    pais = models.ForeignKey(pais, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=100)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.descripcion
    
class ciudad(models.Model):
    provincia = models.ForeignKey(provincia, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=50)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.descripcion
    
class codigo_telefono(models.Model):
    pais = models.ForeignKey(pais, on_delete=models.CASCADE) 
    numero = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.numero} ({self.pais.descripcion})"
    
class tipo_identificacion(models.Model):
    descripcion = models.CharField(max_length=30)

    def __str__(self):
        return self.descripcion
    
    
class registro_historico(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    actividad = models.CharField(max_length=200, null=True, blank=True)
    fecha_registro = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Perfil de {self.usuario.username}"
    
class usuario_detalle(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    tipo_identificacion = models.ForeignKey(tipo_identificacion, on_delete=models.CASCADE)
    identificacion = models.CharField(max_length=50) 
    fecha_nacimiento = models.DateField(null=True, blank=True)
    direccion = models.CharField(max_length=200, null=True, blank=True)
    pais = models.ForeignKey(pais, on_delete=models.CASCADE)
    provincia = models.ForeignKey(provincia, on_delete=models.CASCADE)
    ciudad = models.ForeignKey(ciudad, on_delete=models.CASCADE, blank=True, null=True)
    email2 = models.EmailField(max_length=254, blank=True, null=True)  
    telefono1_codigo = models.ForeignKey(codigo_telefono, on_delete=models.CASCADE, related_name='telefono1_codigo', blank=True, null=True)
    telefono1_numero = models.CharField(max_length=20, blank=True, null=True)  
    telefono2_codigo = models.ForeignKey(codigo_telefono, on_delete=models.CASCADE, related_name='telefono2_codigo', blank=True, null=True)
    telefono2_numero = models.CharField(max_length=20, blank=True, null=True) 

    def __str__(self):
        return f"Perfil de {self.usuario.username}"


    