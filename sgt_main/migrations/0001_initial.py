# Generated by Django 5.0.4 on 2024-08-04 01:01

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='pais',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
                ('estado', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='tipo_identificacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='codigo_telefono',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.CharField(max_length=20)),
                ('pais', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgt_main.pais')),
            ],
        ),
        migrations.CreateModel(
            name='provincia',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
                ('estado', models.BooleanField(default=True)),
                ('pais', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgt_main.pais')),
            ],
        ),
        migrations.CreateModel(
            name='ciudad',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=50)),
                ('estado', models.BooleanField(default=True)),
                ('provincia', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgt_main.provincia')),
            ],
        ),
        migrations.CreateModel(
            name='registro_historico',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('actividad', models.CharField(blank=True, max_length=200, null=True)),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='usuario_detalle',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identificacion', models.CharField(max_length=50)),
                ('fecha_nacimiento', models.DateField(blank=True, null=True)),
                ('direccion', models.CharField(blank=True, max_length=200, null=True)),
                ('email2', models.EmailField(blank=True, max_length=254, null=True)),
                ('telefono1_numero', models.CharField(blank=True, max_length=20, null=True)),
                ('telefono2_numero', models.CharField(blank=True, max_length=20, null=True)),
                ('ciudad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sgt_main.ciudad')),
                ('pais', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgt_main.pais')),
                ('provincia', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgt_main.provincia')),
                ('telefono1_codigo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='telefono1_codigo', to='sgt_main.codigo_telefono')),
                ('telefono2_codigo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='telefono2_codigo', to='sgt_main.codigo_telefono')),
                ('tipo_identificacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sgt_main.tipo_identificacion')),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
