"""
URL configuration for sgt_main project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views
from .views import activate_account
#from tramites import views
#from mantenimientos import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path("home/", views.home, name='home'),
    path("", views.login_view, name='login'),
    path("signup/", views.signup_view, name='signup'),
    path("logout/", views.logout, name='logout'),
    path("settings-profile/", views.settingsProfile, name='settings-profile'),
    path("password_reset/", views.password_reset_view, name='password_reset'),
    path('update_profile/', views.update_profile, name='update_profile'),
    path('update_profile_email/', views.update_profile_email, name='update_profile_email'),
    path('update_profile_phone/', views.update_profile_phone, name='update_profile_phone'),
    path('audit/', views.audit, name='audit'),
    path('send_email/', views.send_email, name='send_email'),
    path('send-test-email/', views.send_test_email, name='send_test_email'),
    path('tramites/', include('tramites.urls')),
    path('mantenimientos/', include('mantenimientos.urls')),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('company/', views.company, name='company'),
    path('about/', views.about, name='about'),
    path('info_contact/', views.info_contact, name='info_contact'),
    path('generate_another_plot/', views.generate_another_plot, name='generate_another_plot'),
    path('generate_another_plot_pastel/', views.generate_another_plot_pastel, name='generate_another_plot_pastel'),
    path('generate_another_plot_area/', views.generate_another_plot_area, name='generate_another_plot_area'),
    path('generate_another_plot_donut/', views.generate_another_plot_donut, name='generate_another_plot_donut'),
    path('generate_stacked_bar_chart/', views.generate_stacked_bar_chart, name='generate_stacked_bar_chart'),
    path('activate/<uidb64>/<token>/', activate_account, name='activate_account'),
]
