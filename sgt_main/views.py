from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate as auth_authenticate
from django.contrib import messages
from .models import pais, provincia, ciudad, codigo_telefono, tipo_identificacion, usuario_detalle, registro_historico
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.http import HttpResponse
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import io
import matplotlib.ticker as ticker
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_str
from django.template.loader import render_to_string
# Create your views here.

def home(request):
    return render(request, 'home.html')

def login_view(request):
    if request.method == 'POST':
        email = request.POST.get('emailAddress')
        password = request.POST.get('loginPassword')
        user = auth_authenticate(request, username=email, password=password)

        if user is not None:
            auth_login(request, user)
            print(f'Usuario autenticado: {user.id}')
            request.session['logged_in'] = True
            request.session['id'] = user.id
            request.session['name'] = user.first_name + user.last_name
            
            registro = registro_historico.objects.create(
                usuario=user,
                actividad='Inicio de sesión',
            )

            return redirect('home')
        else:
            messages.error(
                request, 'Correo electrónico o contraseña incorrectos.')
            return redirect('login')

    return render(request, 'login.html')

def logout(request):
    auth_logout(request)
    request.session.flush()
    return redirect('login')


def settingsProfile(request):
    if request.user.is_authenticated:
        user = request.user
        profile = usuario_detalle.objects.select_related(
            'tipo_identificacion').get(usuario=user)

        context = {
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'is_active': user.is_active,
            'identificacion': profile.identificacion,
            'fecha_nacimiento': profile.fecha_nacimiento,
            'direccion': profile.direccion,
            'email2': profile.email2,
            'telefono1_numero': profile.telefono1_numero,
            'telefono2_numero': profile.telefono2_numero,
            'tipo_identificacion_name': profile.tipo_identificacion.descripcion,
            'tipo_identificacion_profile': profile.tipo_identificacion,
        }
        print(f'tipo_identificacion_name: {profile.tipo_identificacion}')
        pais_d = profile.pais.descripcion if profile.pais else None
        provincia_d = profile.provincia.descripcion if profile.provincia else None
        ciudad_d = profile.ciudad.descripcion if profile.ciudad else None
        context['pais'] = pais_d
        context['provincia'] = provincia_d
        context['ciudad'] = ciudad_d

        pais_id = profile.pais.id if profile.pais else None
        provincia_id = profile.provincia.id if profile.provincia else None
        ciudad_id = profile.ciudad.id if profile.ciudad else None
        context['pais_id'] = pais_id
        context['provincia_id'] = provincia_id
        context['ciudad_id'] = ciudad_id

        pais_list = pais.objects.filter(estado=True).order_by('descripcion')
        context['pais_descripcion'] = pais_list

        provincia_list = provincia.objects.filter(estado=True).order_by('descripcion')
        context['provincia_descripcion'] = provincia_list

        ciudad_list = ciudad.objects.filter(estado=True).order_by('descripcion')
        context['ciudad_descripcion'] = ciudad_list

        codigo_celular = codigo_telefono.objects.filter(pais_id=8)
        context['codigo_celular'] = codigo_celular

        tipo_identificacion_list = tipo_identificacion.objects.filter(descripcion=profile.tipo_identificacion)
        context['tipo_identificacion'] = tipo_identificacion_list

        return render(request, 'settings-profile.html', context)
    else:
        return redirect('login')


def signup_view(request):
    tipo_ident = tipo_identificacion.objects.all() 

    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        emailA = request.POST['emailAddress']
        password = request.POST['loginPassword']
        confirm_password = request.POST['confirmPassword']
        tipo_identificacion_id = request.POST['tipo_identificacion_name']
        identificacion = request.POST['identificacion']
        tipo_identificacion_obj = tipo_identificacion.objects.get(id=tipo_identificacion_id)
        pais_obj = pais.objects.get(id=8)
        provincia_obj = provincia.objects.get(id=10)

        if password != confirm_password:
            messages.error(request, 'Las contraseñas no coinciden.')
            return render(request, 'signup.html', {'tipo_ident': tipo_ident})
        
        if User.objects.filter(email=emailA).exists():
            messages.error(request, 'El correo electrónico ya está registrado.')
            return render(request, 'signup.html', {'tipo_ident': tipo_ident})
        
        if usuario_detalle.objects.filter(identificacion=identificacion).exists():
            messages.error(request, 'La identificación ya existe.')
            return render(request, 'signup.html', {'tipo_ident': tipo_ident})

        user = User.objects.create_user(
            username=emailA,
            email=emailA,
            password=password,
            first_name=first_name,
            last_name=last_name
        )
        
        # Verificar si el objeto usuario_detalle ya existe
        usuario_detalle_obj = usuario_detalle.objects.filter(usuario=user).first()
        if usuario_detalle_obj is None:
            # Si no existe, crear un nuevo objeto usuario_detalle
            usuario_detalle_obj = usuario_detalle.objects.create(
                usuario=user,
                tipo_identificacion=tipo_identificacion_obj,
                identificacion=identificacion,
                fecha_nacimiento=None,
                direccion=None,
                pais=pais_obj,
                provincia=provincia_obj,
                ciudad=None,
                email2=None,
                telefono1_codigo=None,
                telefono1_numero=None,
                telefono2_codigo=None,
                telefono2_numero=None
            )
        else:
            # Si ya existe, actualizar el objeto usuario_detalle existente
            usuario_detalle_obj.tipo_identificacion = tipo_identificacion_obj
            usuario_detalle_obj.identificacion = identificacion
            usuario_detalle_obj.fecha_nacimiento = None
            usuario_detalle_obj.direccion = None
            usuario_detalle_obj.pais = pais_obj
            usuario_detalle_obj.provincia = provincia_obj
            usuario_detalle_obj.ciudad = None
            usuario_detalle_obj.email2 = None
            usuario_detalle_obj.telefono1_codigo = None
            usuario_detalle_obj.telefono1_numero = None
            usuario_detalle_obj.telefono2_codigo = None
            usuario_detalle_obj.telefono2_numero = None
            usuario_detalle_obj.save()
        
        registro_historico.objects.create(
            usuario=user,
            actividad='Registro de Usuario',
        )
        
        user.save()
        auth_login(request, user)
        messages.success(request, 'Usuario creado exitosamente. ¡Bienvenido!')
        return redirect('/')
    
    return render(request, 'signup.html', {'tipo_ident': tipo_ident})

def password_reset_view(request):
    if request.method == 'POST':
        email = request.POST['emailAddress']
        password = request.POST['loginPassword']
        confirm_password = request.POST['confirmPassword']

        if password != confirm_password:
            messages.error(request, 'Las contraseñas no coinciden.')
        else:
            try:
                user = User.objects.get(email=email)
                user.set_password(password)
                user.is_active = False
                registro = registro_historico.objects.create(
                    usuario=user,
                    actividad='Reinicio de contraseña',
                    )
                user.save()
                messages.success(request, 'Contraseña actualizada correctamente. Se ha enviado un correo con el enlace de activación.')

                 # Generar token y UID para el enlace
                token = default_token_generator.make_token(user)
                uid = urlsafe_base64_encode(force_bytes(user.pk))

                # Construir el enlace de activación
                current_site = get_current_site(request)
                activation_link = reverse('activate_account', kwargs={'uidb64': uid, 'token': token})
                activation_url = f"http://{current_site.domain}{activation_link}"
                asunto = "Cambio de contraseña"
                cuerpo = f'Estimado usuario, usted ha cambiado su contraseña. Por favor, ingrese al siguiente enlace para activar su cuenta: {activation_url}'
                send_mail(asunto, cuerpo,
                        'luis.pilay@gizlocorp.com', [email])
                return redirect('/')
            except User.DoesNotExist:
                messages.error(request, 'El correo electrónico no está registrado.')

    return render(request, 'password_reset.html')

def activate_account(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True  # Reactivar cuenta
        user.save()
        messages.success(request, 'Cuenta activada correctamente. Ya puede iniciar sesión.')
        return redirect('login')  # Redirige a la página de inicio de sesión
    else:
        messages.error(request, 'El enlace de activación es inválido.')
        return redirect('/')

def update_profile(request):
    if request.method == 'POST':
        user = request.user
        profile = user.usuario_detalle

        user.first_name = request.POST['firstName']
        user.last_name = request.POST['lastName']
        user.save()

        print(f'tipo_identificacion_name: {request.POST['pais_id']}')

        registro = registro_historico.objects.create(
                    usuario=user,
                    actividad='Actualización de datos personales',
                    )

        profile.direccion = request.POST['address']
        profile.pais_id = request.POST['pais_id']
        profile.provincia_id = request.POST['provincia_id']
        profile.ciudad_id = request.POST['ciudad_id']
        profile.fecha_nacimiento = request.POST['birthDate']
        profile.save()

        return redirect('settings-profile')  
    else:
        return render(request, 'settings-profile.html')  
    

def update_profile_email(request):
    if request.method == 'POST':
        user = request.user
        profile = user.usuario_detalle

        profile.email2 = request.POST['email2']
        profile.save()

        registro = registro_historico.objects.create(
                    usuario=user,
                    actividad='Actualización de datos personales',
                    )

        return redirect('settings-profile')  
    else:
        return render(request, 'settings-profile.html')  
    
def update_profile_phone(request):
    if request.method == 'POST':
        user = request.user
        profile = user.usuario_detalle

        profile.telefono1_numero = request.POST['phone1']
        profile.telefono2_numero = request.POST['phone2']
        profile.save()

        registro = registro_historico.objects.create(
                    usuario=user,
                    actividad='Actualización de datos personales',
                    )

        return redirect('settings-profile')  
    else:
        return render(request, 'settings-profile.html') 
    
def send_test_email(request): 
    if request.method == 'POST':
        user = request.user
        asunto = request.POST['asuntocorreo']
        cuerpocorreo = request.POST['cuerpocorreo']
        correo = request.POST['emailcorreo']
        print(f'correo: {correo}')
    try:
        send_mail(asunto, 
              cuerpocorreo, 
              'luis.pilay@gizlocorp.com', [correo])
        
        registro = registro_historico.objects.create(
                    usuario=user,
                    actividad='Envio de correo desde Comunicaciones',
                    )
        messages.success(request, 'Correo enviado exitosamente.')
        return redirect('info_contact')
    except Exception as e:
        messages.error(request, 'El correo electrónico no está registrado.')
    return render(request, 'info_contact.html')

def audit(request):
    registros = list(registro_historico.objects.select_related('usuario').order_by('-fecha_registro'))
    return render(request, 'audit.html', {'registros': registros})

def send_email(request):
    return render(request, 'send_email.html')

def dashboard(request):
    return render(request, 'dashboard.html')

def generate_another_plot(request):
    # Obtener los datos del modelo y procesarlos
    registros = registro_historico.objects.filter(actividad="Registro de Usuario").values('fecha_registro')
    df = pd.DataFrame(registros)
    df['fecha_registro'] = pd.to_datetime(df['fecha_registro'])
    df['mes'] = df['fecha_registro'].dt.to_period('M')
    
    # Agrupar por mes y contar registros
    df_count = df.groupby('mes').size().reset_index(name='count')

    # Generar la gráfica
    plt.figure(figsize=(10, 6))
    plt.bar(df_count['mes'].astype(str), df_count['count'], color='skyblue')
    plt.title('Registro de Usuario')
    plt.xlabel('Mes')
    plt.ylabel('# Usuarios')
    plt.xticks(rotation=45)
    plt.grid(True)

    plt.yticks(range(min(df_count['count']), max(df_count['count']) + 1))

    # Guardar la gráfica en un objeto BytesIO en lugar de un archivo
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    return HttpResponse(buf, content_type='image/png')

def generate_another_plot_pastel(request):
    # Obtener los datos del modelo y procesarlos
    registros = registro_historico.objects.filter(actividad="Inicio de sesión").values('fecha_registro')
    df = pd.DataFrame(registros)
    df['fecha_registro'] = pd.to_datetime(df['fecha_registro'])
    df['mes'] = df['fecha_registro'].dt.to_period('M')
    
    # Agrupar por mes y contar registros
    df_count = df.groupby('mes').size().reset_index(name='count')

    # Generar la gráfica
    plt.figure(figsize=(10, 6))
    plt.pie(df_count['count'], labels=df_count['mes'].astype(str), autopct='%1.1f%%')
    plt.title('Inicio de Sesión')
    plt.axis('equal')  # Para que el círculo sea perfecto

    # Guardar la gráfica en un objeto BytesIO en lugar de un archivo
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    return HttpResponse(buf, content_type='image/png')

def generate_another_plot_area(request):
    # Obtener los datos del modelo y procesarlos
    registros = registro_historico.objects.filter(actividad="Trámite Permiso de Circulación").values('fecha_registro')
    df = pd.DataFrame(registros)
    df['fecha_registro'] = pd.to_datetime(df['fecha_registro'])
    df['mes'] = df['fecha_registro'].dt.to_period('M')
    
    # Agrupar por mes y contar registros
    df_count = df.groupby('mes').size().reset_index(name='count')

    # Generar la gráfica
    plt.figure(figsize=(10, 6))
    plt.fill_between(range(len(df_count)), df_count['count'].astype(int), alpha=0.5)
    plt.title('Trámite Permiso de Circulación')
    plt.xlabel('Mes')
    plt.ylabel('# Usuarios')
    plt.xticks(range(len(df_count)), df_count['mes'].astype(str), rotation=45)
    plt.yticks(range(max(df_count['count']) + 1))
    plt.grid(True)

    # Guardar la gráfica en un objeto BytesIO en lugar de un archivo
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    return HttpResponse(buf, content_type='image/png')

def generate_another_plot_donut(request):
    # Obtener los datos del modelo y procesarlos
    registros = registro_historico.objects.filter(actividad="Trámite Impugnación de Multas").values('fecha_registro')
    df = pd.DataFrame(registros)

    # Comprobar si el DataFrame tiene la columna 'fecha_registro'
    if 'fecha_registro' in df.columns:
        df['fecha_registro'] = pd.to_datetime(df['fecha_registro'])
        df['mes'] = df['fecha_registro'].dt.to_period('M')
        
        # Agrupar por mes y contar registros
        df_count = df.groupby('mes').size().reset_index(name='count')

        # Generar la gráfica
        plt.figure(figsize=(10, 6))
        plt.pie(df_count['count'], labels=df_count['mes'], autopct='%1.1f%%', startangle=90)
        centre_circle = plt.Circle((0, 0), 0.70, fc='white')
        fig = plt.gcf()
        fig.gca().add_artist(centre_circle)
        plt.axis('equal')  
        plt.title('Trámite Impugnación de Multas')

        # Guardar la gráfica en un objeto BytesIO en lugar de un archivo
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)

        return HttpResponse(buf, content_type='image/png')
    else:
        # Si el DataFrame no tiene la columna 'fecha_registro', devuelve un mensaje de error
        return HttpResponse("No hay datos disponibles para generar la gráfica.", status=404)

def generate_stacked_bar_chart(request):
    # Obtener los datos del modelo y procesarlos
    registros_actualizacion = registro_historico.objects.filter(actividad="Vinculación de Socios y Vehículos").values('fecha_registro')
    registros_registro = registro_historico.objects.filter(actividad="Desvinculación de Socios y Vehículos").values('fecha_registro')
    
    df_actualizacion = pd.DataFrame(registros_actualizacion)
    df_registro = pd.DataFrame(registros_registro)
    
    df_actualizacion['fecha_registro'] = pd.to_datetime(df_actualizacion['fecha_registro'])
    df_registro['fecha_registro'] = pd.to_datetime(df_registro['fecha_registro'])
    
    df_actualizacion['mes'] = df_actualizacion['fecha_registro'].dt.to_period('M')
    df_registro['mes'] = df_registro['fecha_registro'].dt.to_period('M')
    
    # Agrupar por mes y contar registros
    df_count_actualizacion = df_actualizacion.groupby('mes').size().reset_index(name='count_actualizacion')
    df_count_registro = df_registro.groupby('mes').size().reset_index(name='count_registro')
    
    # Generar la gráfica
    plt.figure(figsize=(10, 6))
    plt.bar(df_count_actualizacion['mes'].astype(str), df_count_actualizacion['count_actualizacion'], label='Vinculación de Socios y Vehículos')
    plt.bar(df_count_registro['mes'].astype(str), df_count_registro['count_registro'], bottom=df_count_actualizacion['count_actualizacion'], label='Desvinculación de Socios y Vehículos')
    plt.title('Socios y Vehículos')
    plt.xlabel('Mes')
    plt.ylabel('# Usuarios')
    plt.xticks(rotation=45)
    plt.legend()
    plt.grid(True)

    plt.ylim(0, max(df_count_actualizacion['count_actualizacion']) + 1)
    yticks = range(max(df_count_actualizacion['count_actualizacion']) + 1)
    plt.yticks(yticks)

    # Guardar la gráfica en un objeto BytesIO en lugar de un archivo
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    return HttpResponse(buf, content_type='image/png')

def company(request):
    return render(request, 'company.html')

def about(request):
    return render(request, 'about.html')

def info_contact(request):
    return render(request, 'info_contact.html')
