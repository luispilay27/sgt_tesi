# seed_data.py

import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sgt_main.settings')
django.setup()

from mantenimientos.models import Dias,Horas, Actividades, Configuraciones
from tramites.models import Tipos_tramite, Requisitos_Tramites
from sgt_main.models import tipo_identificacion

def seed():
    # Datos a insertar
    dataDias = [
        {'nombre': 'Lunes'},
        {'nombre': 'Martes'},
        {'nombre': 'Miércoles'},
        {'nombre': 'Jueves'},
        {'nombre': 'Viernes'},
        {'nombre': 'Sábado'},
        {'nombre': 'Domingo'},
    ]

    for item in dataDias:
        Dias.objects.create(**item)
    print("Data seeded Dias successfully!")

    dataHoras = [
        {'hora': '00:00'}, {'hora': '01:00'}, {'hora': '02:00'},{'hora': '03:00'}, {'hora': '04:00'}, {'hora': '05:00'},
        {'hora': '06:00'}, {'hora': '06:00'}, {'hora': '08:00'},{'hora': '09:00'}, {'hora': '10:00'}, {'hora': '11:00'},
        {'hora': '12:00'}, {'hora': '13:00'}, {'hora': '14:00'},{'hora': '15:00'}, {'hora': '16:00'}, {'hora': '17:00'},
        {'hora': '18:00'}, {'hora': '19:00'}, {'hora': '20:00'},{'hora': '21:00'}, {'hora': '22:00'}, {'hora': '23:00'},
    ]

    for item in dataHoras:
        Horas.objects.create(**item) 
    print("Data seeded Horas successfully!")

    dataConfiguraciones = [
        {'clave': 'tonelaje', 
         'valor': '2.5', 
         'descripcion':'Valida si el vehiculo necesita permiso de circulacion',
         'indicacion':'ingresar el valor con punto para los decimales'},
    ]

    for item in dataConfiguraciones:
        Configuraciones.objects.create(**item) 
    print("Data seeded Configuraciones successfully!")

    dataActividades = [
        {'nombre': 'Alimentos'  , 'codigo': '001'},
        {'nombre': 'Tecnología' , 'codigo': '002'},
    ]

    for item in dataActividades:
        Actividades.objects.create(**item) 

    print("Data seeded Actividades successfully!")

    dataTipoTramite = [
        {'nombre': 'IMPUGNACION DE MULTAS' , 'descripcion':'IMPUGNACION DE MULTAS'},
        {'nombre': 'PERMISO DE CIRCULACION', 'descripcion': 'PERMISO DE CIRCULACION'},
        {'nombre': 'SOCIOS Y VEHICULOS', 'descripcion': 'SOCIOS Y VEHICULOS'},
    ]

    for item in dataTipoTramite:
        Tipos_tramite.objects.create(**item) 

    print("Data seeded Tipos Tramites successfully!")

    dataTipoIdentificacion = [
        {'descripcion': 'CEDULA' },
        {'descripcion': 'RUC'},
    ]

    for item in dataTipoIdentificacion:
        tipo_identificacion.objects.create(**item) 

    print("Data seeded Tipos Identificacion successfully!")

    dataRequisitos = [
        {'tipo_id': 3, 'nombre': 'Solicitud dirigida al Gerente General', 'descripcion':'Solicitud dirigida al Gerente General','tipo_tramite_socio_vehiculo':'vinculacion'},
        {'tipo_id': 3, 'nombre': 'Formulario de Solicitud de Incremento de Cupo', 'descripcion':'Formulario de Solicitud de Incremento de Cupo','tipo_tramite_socio_vehiculo':'vinculacion'},
        {'tipo_id': 3, 'nombre': 'Solicitud dirigida al Gerente General', 'descripcion':'Solicitud dirigida al Gerente General','tipo_tramite_socio_vehiculo':'desvinculacion'},
        {'tipo_id': 3, 'nombre': 'Formulario de Solicitud de Desvinculación', 'descripcion':'Formulario de Solicitud de Desvinculacion','tipo_tramite_socio_vehiculo':'desvinculacion'},        
    ]
    """
    {'tipo_id': 3, 'nombre': 'Comprobante de pago del servicio', 'descripcion':'Comprobante de pago del servicio'},
        {'tipo_id': 3, 'nombre': 'Copia del RUC a color actualizado', 'descripcion':'Copia del RUC a color actualizado'},
    {'tipo_id': 3, 'nombre': 'Copia del Nombramiento del representante legal', 'descripcion':'Copia del Nombramiento del representante legal inscrito en el registro mercantil, copia de la cédula y papeleta de votación vigente del representante legal'},
        {'tipo_id': 3, 'nombre': 'Certificado de cumplimientos de obligaciones con el IESS de la operadora', 'descripcion':'Certificado de cumplimientos de obligaciones con el IESS de la operadora'},
        {'tipo_id': 3, 'nombre': 'Listado actualizado de accionistas', 'descripcion':'Listado actualizado (último 30 días) de accionistas emitido por la Superintendencia de Compañías'},
        {'tipo_id': 3, 'nombre': 'Copia de la cédula y papeleta de votación de los socios propuestos', 'descripcion':'Copia de la cédula y papeleta de votación de los socios propuestos'},
        {'tipo_id': 3, 'nombre': 'Certificado de historial laboral del IESS de el o los nuevos socios y/o accionistas de la compañía o cooperativa', 'descripcion':'Certificado de historial laboral del IESS de el o los nuevos socios y/o accionistas de la compañía o cooperativa, para verificar que no sean autoridades o empleados civiles que trabajen o hayan trabajado hasta dos años antes de la fecha de la solicitud, en los organismos relacionados con el tránsito y el transporte terrestre, según la Disposición General Decimoctava de la LOTTTSV. Si el o los vehículos son de propiedad de la operadora, el certificado del historial laboral del IESS corresponderá al del representante legal'},
        {'tipo_id': 3, 'nombre': 'Copia de la licencia profesional del socio o chofer que manejará el vehículo', 'descripcion':'Copia de la licencia profesional del socio o chofer que manejará el vehículo'},
        {'tipo_id': 3, 'nombre': 'Certificado de Comandancia de Policía y Comisión de Tránsito del Ecuador de NO PERTENECER a la fuerza pública', 'descripcion':'Certificado de Comandancia de Policía y Comisión de Tránsito del Ecuador de NO PERTENECER a la fuerza pública'},
        {'tipo_id': 3, 'nombre': 'Certificado de comandancia de las Fuerzas Armadas de NO PERTENECER a la fuerza armada', 'descripcion':'Certificado de comandancia de las Fuerzas Armadas de NO PERTENECER a la fuerza armada'},
        {'tipo_id': 3, 'nombre': 'Copia de la matrícula vigente', 'descripcion':'Copia de la matrícula vigente'},
        {'tipo_id': 3, 'nombre': 'Copia Permiso de operación', 'descripcion':'Copia Permiso de operación y de todas las resoluciones de los socios'},
        {'tipo_id': 3, 'nombre': 'Certificado de no adeudar al GADMS', 'descripcion':'Certificado de no adeudar al GADMS'},
        {'tipo_id': 3, 'nombre': 'Copia de la factura del o los vehículos nuevos; copia de matrícula para vehículos usados; o Carta de Intención (formato ATM) en el cual el nuevo socio se comprometa ', 'descripcion':'Copia de la factura o contrato de compra venta notariado del o los vehículos nuevos; copia de matrícula para vehículos usados; o Carta de Intención (formato ATM) en el cual el nuevo socio se comprometa a ingresar un vehículo en la Operadora cumpliendo la normativa vigente'},
        {'tipo_id': 3, 'nombre': 'Revisión técnica vehicular vigente', 'descripcion':'Revisión técnica vehicular vigente, según el cuadro de calendarización vigente, excepto para vehículos nuevos'},
        {'tipo_id': 3, 'nombre': 'Carta justificativa del aumento de cupo', 'descripcion':'Carta justificativa del aumento de cupo, detallando las razones que respaldan esta solicitud'},
        {'tipo_id': 3, 'nombre': 'Carta de Intención', 'descripcion':'Carta de Intención'},

    """
    for item in dataRequisitos:
        Requisitos_Tramites.objects.create(**item) 

    print("Data seeded dataRequisitos successfully!")

    print("successfully!")

if __name__ == '__main__':
    seed()
