from django.db import models
class Configuraciones(models.Model):
    clave = models.CharField(max_length=250)
    valor = models.CharField(max_length=250)
    descripcion = models.CharField(max_length=250)
    indicacion = models.CharField(max_length=250)
    creacion = models.DateTimeField(auto_now_add=True)
    actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.clave
    
class Actividades(models.Model):
    OPCIONES_ESTADO = [                
        ('A', 'ACTIVO'),
        ('I', 'INACTIVO'),
    ]
    nombre = models.CharField(max_length=250)
    codigo = models.CharField(max_length=5)
    estado = models.CharField(
        max_length=1,
        choices=OPCIONES_ESTADO,
        default='A', 
    )

    def __str__(self):
        return self.nombre
    
class Dias(models.Model):
    OPCIONES_ESTADO = [                
        ('A', 'ACTIVO'),
        ('I', 'INACTIVO'),
    ]
    nombre = models.CharField(max_length=10)  # Lunes, Martes, etc.
    estado = models.CharField(
        max_length=1,
        choices=OPCIONES_ESTADO,
        default='A', 
    )
    creacion = models.DateTimeField(auto_now_add=True)
    actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.nombre
class Horas(models.Model):
    OPCIONES_ESTADO = [                
        ('A', 'ACTIVO'),
        ('I', 'INACTIVO'),
    ]
    hora = models.TimeField()  # Ejemplo: 08:00, 09:00, etc.
    estado = models.CharField(
            max_length=1,
            choices=OPCIONES_ESTADO,
            default='A', 
        )
    creacion = models.DateTimeField(auto_now_add=True)
    actualizacion = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.hora.strftime('%H:%M')
class Actividad_horarios(models.Model):
    OPCIONES_ESTADO = [                
        ('A', 'ACTIVO'),
        ('I', 'INACTIVO'),
    ]
    actividad= models.ForeignKey(Actividades, on_delete=models.CASCADE)
    dia = models.ForeignKey(Dias, on_delete=models.CASCADE)
    hora = models.ForeignKey(Horas, related_name='actividad_hora', on_delete=models.CASCADE)
    estado = models.CharField(
            max_length=1,
            choices=OPCIONES_ESTADO,
            default='A', 
        )
    creacion = models.DateTimeField(auto_now_add=True)
    actualizacion = models.DateTimeField(auto_now=True)
    usuario_creacion = models.CharField(max_length=50) 
    usuario_actualizacion = models.CharField(max_length=50) 

    def __str__(self):
        return self.actividad

