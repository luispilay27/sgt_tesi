from django.urls import path, include
from mantenimientos import views

urlpatterns = [
    path('actividades/', views.actividades_index, name='actividades'), 
    path('actividad/actualizar-estado/', views.actualizarEstadoActividad, name='actividad_actualizar_estado'),
    path('actividad/horario/nuevo/', views.consultarActvidades, name='actividad_horario_nuevo'),
    path('actividad/horario/<int:id>/', views.consultarActvidadHorario, name='actividad_horario_editar'),
    path('actividad/horarios/consultar/', views.consultarActvidades, name='actividad_horario_consultar'),
    path('actividad/horarios/guardar', views.guardarHorariosActividad, name='actividad_horario_guardar'),

]
