from django.contrib import admin
from .models import Dias,Horas, Configuraciones, Actividades

admin.site.register(Dias)
admin.site.register(Horas)
admin.site.register(Configuraciones)
admin.site.register(Actividades)