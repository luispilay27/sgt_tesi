import requests
from django.shortcuts import render
from django.http import JsonResponse
from django.db.models import Subquery, OuterRef
from .models import Configuraciones, Dias, Horas, Actividades, Actividad_horarios
from django.views.decorators.csrf import csrf_exempt
import json


def actividades_index(request):    
    actividad_horarios = (
    Actividad_horarios.objects
    .select_related('actividad')  
    .values('actividad__id', 'actividad__nombre', 'actividad__estado') 
    .distinct() 
    )
    print(actividad_horarios)
    return render(request, 'actividades/index.html', {'actividades': actividad_horarios})

def actualizarEstadoActividad(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        nuevo_estado = request.POST.get('estado')

        try:
            actividades = Actividades.objects.get(id=id)
            actividades.estado = nuevo_estado
            actividades.save()
            return JsonResponse({'success': True, 'nuevo_estado': nuevo_estado})
        except Actividades.DoesNotExist:
            return JsonResponse({'success': False, 'error': 'Registro no encontrado'})
    return JsonResponse({'success': False, 'error': 'Método no permitido'})

def consultarActvidades(request):  
    actividades_con_horarios = Actividad_horarios.objects.values_list('actividad__id', flat=True)
    actividades = Actividades.objects.exclude(id__in=Subquery(actividades_con_horarios)).filter( estado="A")    
    dias = Dias.objects.all()
    horas = Horas.objects.all().order_by('hora')
    
    return render( request, 'actividades/nuevo.html', 
                    {'actividades': actividades,
                    'horas':horas,
                    'dias':dias})

def consultarActvidadHorario(request, id):    
    if not id:
        return JsonResponse({'error': 'Actividad no especificada'}, status=400)
    
    actividades = Actividades.objects.all()
    dias = Dias.objects.all()
    horas = Horas.objects.all().order_by('hora')
    actividadHorario = Actividad_horarios.objects.filter(actividad__id=id)

    actividad_data = []
    for hora in horas:
        hora_data = {
            'hora': hora,
            'horaId': hora.id,
            'horariosDias': []
        }
        for dia in dias:
            actividad_horario = actividadHorario.filter( dia=dia, hora=hora).first()
            if actividad_horario:
                estado = actividad_horario.estado  
            else:
                estado = 'I'  
            
            hora_data['horariosDias'].append({
                
                'diaNombre': dia.nombre,
                'diaId': dia.id,
                'estado': estado
            })        
        actividad_data.append(hora_data)

    return render( request, 'actividades/nuevo.html', 
                    {'actividad_id': id,
                    'actividades': actividades,
                    'horas':horas,
                    'dias':dias,
                    'actividad_horarios': actividad_data,})

def guardarHorariosActividad(request):
    if request.method == 'POST':
        selected_horarios = json.loads(request.POST.get('selectedHorarios', '{}'))
        actividad= request.POST.get('actividadId')

        for key, value in selected_horarios.items():
            dia, hora = key.split('][')
            dia = dia.replace('horarios[', '')
            hora = hora.replace(']', '')
            
            objActividad = Actividades.objects.get(id=actividad)
            objDia = Dias.objects.get(id=dia)
            objHora = Horas.objects.get(id=hora)

            Actividad_horarios.objects.create(actividad = objActividad, dia=objDia, hora=objHora, estado='A')

        return JsonResponse({'success': True,'message': 'Horarios guardados exitosamente'})
    return JsonResponse({'success': False,'error': 'Método no permitido'}, status=405)
