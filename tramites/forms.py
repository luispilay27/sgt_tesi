from django import forms
from .models import Tramite_socios_vehiculos

class RequisitoForm(forms.ModelForm):
    class Meta:
        model = Tramite_socios_vehiculos
        fields = ['archivos']
        exclude = ['tramite', 'tipo', 'requisito','creacion','actualizacion' 'usuario'] 
        
