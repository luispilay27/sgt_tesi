import requests
import json
import os
from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import JsonResponse,HttpResponse
from django.conf import settings
from reportlab.lib.pagesizes import letter
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.lib.colors import HexColor
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table, TableStyle
from reportlab.pdfgen import canvas
from django.db.models import Subquery
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from mantenimientos.models import Configuraciones, Actividades, Actividad_horarios
from sgt_main.models import usuario_detalle
from .models import Tokens, Tramites, Tramite_multa_impugnar, Api_consumos_bitacora, Tramite_permiso_circulacion, Tramite_socios_vehiculos, Requisitos_Tramites
from .forms import RequisitoForm
from sgt_main.models import registro_historico
from django.core.mail import send_mail
from django.contrib import messages
from django.core.mail import EmailMessage
from django.http import FileResponse
from django.shortcuts import get_object_or_404

def multas(request):
    return render(request, 'multas/index.html')


def multasImpugnadas(request):
    return render(request, 'multas/impugnaciones/index.html')


def multasConsultar(request, placa):
    estados = [
        ('A', 'ACTIVO'),
        ('I', 'INACTIVO'),
        ('RS', 'REVISIÓN DE SOLICITUD'),
        ('PP', 'PENDIENTE DE PAGO'),
        ('P', 'PAGADO'),
        ('R', 'RECHAZADO'),
        ('AP', 'APROBADO'),
    ]
    estados_dict = dict(estados)
    if not placa:
        return JsonResponse({'error': 'Parámetro placa no proporcionado'}, status=400)

    validarAutentificacion()
    api_url = os.getenv("API_URL")+"api/citacion/consult/"
    data = {
        'placa': placa,
    }
    response_data = consume_api(
        api_url, method='POST', data=data, modulo='multas/'+placa)
    multas =  response_data['data']['aprobada']
    for multa in multas:
        numero = multa.get('numero')
        tramite = Tramite_multa_impugnar.objects.filter(citacion=numero)[:1]
        if tramite:
            multa['existe_solicitud'] = 1
        else:
            multa['existe_solicitud'] = 0
    multas = response_data['data']['impugnada']
    for multa in multas:
        numero = multa.get('numero')
        tramite = Tramite_multa_impugnar.objects.filter(
            citacion=numero).select_related('tramite').first()
        if tramite:
            multa['estadoCod'] = tramite.tramite.estado
            multa['tramite_id'] = tramite.tramite.id
            multa['tramite_solicitud_atv'] = tramite.tramite.solicitud_atv
            multa['estado_descripcion'] = estados_dict.get(
                multa['estadoCod'], 'Estado desconocido')
            multa['estado'] = multa['estado_descripcion']
        else:
            multa['estado'] = 'NO ESTÁ EN EL SISTEMA'
    return JsonResponse(response_data, status=response_data['status'])


def multasImpugnar(request):
    
    validarAutentificacion()
    placa = request.POST.get('placa')
    print(f'placaa: {placa}')
    print(f'user: {request.user.email}')

    tramite = Tramites.objects.create(placa=placa, tipo_id=1, usuario=request.user.email)
    tramite_id = tramite.id
    
    user = request.user
    email = request.user.email
    citacion_numero = request.POST.get('citacion_numero')
    identificacion = request.POST.get('identificacion')
    nombres = request.POST.get('nombres')
    telefono = request.POST.get('telefono')
    correo = request.POST.get('correo')
    mensaje = request.POST.get('mensaje')
    
    Tramite_multa_impugnar.objects.create(tramite_id=tramite_id, citacion=citacion_numero, identificacion=identificacion,
                                         nombres=nombres, telefono=telefono, email=correo, mensaje=mensaje)

    api_url = os.getenv("API_URL")+"api/citacion/impugnacion/"
    data = {
        'citacion_numero': citacion_numero,
        'identificacion': identificacion,
        'name': nombres,
        'phone': telefono,
        'mail': correo,
        'mensaje': mensaje,
    }
    
    response_data = consume_api(
       api_url, method='POST', data=data, modulo='multas/impugnar')

    if response_data['status'] == 200:
        tramite.solicitud_atv = response_data['data']['solicitud']
        tramite.codigo_atv = response_data['data']['code']
        tramite.estado = 'RS'
        tramite.save()
    
    registro_historico.objects.create(
            usuario=user,
            actividad='Trámite Impugnación de Multas',
        )
    asunto = "Ingreso Trámite Impugnación de Multas N. Citación: {}".format(citacion_numero)
    try:
        send_mail(asunto,
                  'Estimado usuario usted ha realizando el ingreso del Trámite Impugnación de Multas',
                  'luis.pilay@gizlocorp.com', [email])

        messages.success(request, 'Correo enviado exitosamente.')
        return redirect('/tramites/multas/')
    except Exception as e:
            messages.error(request, 'El correo electrónico no está registrado.')

    return redirect('/tramites/multas/')

def multasImpugnadasRespuesta(request):
    validarAutentificacion()
    solicitud_atv = '2372-IMP-ATV-2024'
    api_url = os.getenv("API_URL") + \
        "api/citacion/impugnacion/doc/"+solicitud_atv

    response_data = consume_api(api_url, method='GET')

    return JsonResponse(response_data, status=response_data['status'])


def permisoCirculacionIndex(request):
    permisos_circulacion = Tramite_permiso_circulacion.objects.select_related(
        'tramite').all()
    estados = [
        ('A', 'ACTIVO'),
        ('I', 'INACTIVO'),
        ('RS', 'REVISIÓN DE SOLICITUD'),
        ('PP', 'PENDIENTE DE PAGO'),
        ('P', 'PAGADO'),
        ('R', 'RECHAZADO'),
        ('AP', 'APROBADO'),
    ]
    return render(request, 'permiso_circulacion/index.html',
                  {'permisos_circulacion': permisos_circulacion, 'estados': estados})


def permisoCirculacionNuevo(request):
    # Consulta la base de datos
    try:
        configuracion = Configuraciones.objects.filter(
            clave='tonelaje').first()
        actividades_con_horarios = Actividad_horarios.objects.values_list(
            'actividad__id', flat=True)
        actividades = Actividades.objects.filter(
            id__in=Subquery(actividades_con_horarios), estado='A')
        valor_tonelaje_requerido = configuracion.valor

    except Configuraciones.DoesNotExist:
        valor_tonelaje_requerido = 'No disponible'  # O cualquier valor por defecto

    # Pasa el valor a la plantilla
    return render(request, 'permiso_circulacion/nuevo.html',
                  {'valor_tonelaje_requerido': valor_tonelaje_requerido,
                   'actividades': actividades})


def permisoCirculacionSolicitar(request):
    if request.method == 'POST':
        placa = request.POST.get('placa')
        tramite = Tramites.objects.create(placa=placa, tipo_id=2)
        tramite_id = tramite.id

        identificacion = request.POST.get('identificacion')
        tonelaje = request.POST.get('tonelaje')
        actividad = request.POST.get('actividad')

        Tramite_permiso_circulacion.objects.create(tramite_id=tramite_id,
                                                   identificacion=identificacion,
                                                   tonelaje=tonelaje,
                                                   actividad_id=actividad,)
        response_data = {
            "data": {
                "code": "000",
                "message": "OK",
                "solicitud": str(tramite_id)+"-IMP-ATV-2024",
                "error_mail": 0
            },
            "status": 200
        }
        if response_data['status'] == 200:
            tramite.solicitud_atv = response_data['data']['solicitud']
            tramite.codigo_atv = response_data['data']['code']
            tramite.estado = 'RS'
            tramite.save()
            
            response_data = {
                'status': 'success',
                'message': 'Formulario creado correctamente.',
                'redirect_url':  reverse('permiso_circulacion')
            }
            user = request.user
            registro_historico.objects.create(
            usuario=user,
            actividad='Trámite Permiso de Circulación',
            )
            email = request.user.email
            asunto = "Ingreso Trámite Permiso de Circulación N. Solicitud: {}".format(tramite.solicitud_atv)
            try:
                send_mail(asunto,
                        'Estimado usuario usted ha realizando el ingreso del Trámite Permiso de Circulación',
                        'luis.pilay@gizlocorp.com', [email])

            except Exception as e:
                    JsonResponse({'status': 'error', 'message': 'Método no permitido'}, status=405)
            
            return JsonResponse(response_data)
    return JsonResponse({'status': 'error', 'message': 'Método no permitido'}, status=405)


def autentificacion(auth_url, username, password):
    response = requests.post(
        auth_url, data={'username': username, 'password': password})

    if response.status_code == 200:
        token = response.json().get('token')
        if token:
            Tokens.objects.filter(estado='A').update(
                estado='I', actualizacion=timezone.now())

            Tokens.objects.create(token=token)
            return token
        else:
            raise ValueError("Token no encontrado en la respuesta")
    else:
        response.raise_for_status()


def consume_api(url, method='GET', data=None, modulo=''):
    token_obj = Tokens.objects.filter(estado='A').latest('creacion')
    headers = {
        'Authorization': f'Token {token_obj.token}'
    }

    if method.upper() == 'GET':
        response = requests.get(url, headers=headers, params=data)
    elif method.upper() == 'POST':
        response = requests.post(url, headers=headers, data=data)
    else:
        raise ValueError("Método HTTP no soportado")

    bitacora = Api_consumos_bitacora.objects.create(
        url=url, request=data, response='', modulo=modulo)
    if response.status_code == 200:
        status_code = response.status_code
        data = response.json() if response.headers.get(
            'Content-Type') == 'application/json' else response.text
        bitacora.response = data
        bitacora.save()
        return {'status': status_code, 'data': data}
    else:
        data = response.json() if response.headers.get(
            'Content-Type') == 'application/json' else response.text
        bitacora.response = data
        bitacora.save()
        response.raise_for_status()


def validarAutentificacion():
    validarToken = Tokens.objects.filter(estado='A')
    if not validarToken:
        auth_url = os.getenv("API_URL")+"api-token-auth/"
        autentificacion(auth_url, os.getenv("API_USERNAME"),
                        os.getenv("API_PASSWORD"))

estados = [
    ('A', 'ACTIVO'),
    ('I', 'INACTIVO'),
    ('RS', 'REVISIÓN DE SOLICITUD'),
    ('PP', 'PENDIENTE DE PAGO'),
    ('P', 'PAGADO'),
    ('R', 'RECHAZADO'),
    ('AP', 'APROBADO'),
]

# Crea un diccionario que mapee los códigos de estado a sus descripciones
estados_dict = {codigo: descripcion for codigo, descripcion in estados}
@csrf_exempt
def actualizar_estado_tramite(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body.decode('utf-8'))
            solicitud_atv = data.get('solicitud_atv')
            citacion_numero = data.get('citacion_atv')
            nuevo_estado = data.get('estado')
            email = data.get('email')

            if not solicitud_atv or not nuevo_estado:
                return JsonResponse({'success': False, 'message': 'Faltan datos obligatorios'}, status=400)

            tramite = Tramites.objects.get(solicitud_atv=solicitud_atv)

            if Tramites.objects.filter(id=tramite.id).exists():
                tramite.estado = nuevo_estado
                tramite.save()

                # Obtiene la descripción del estado desde el diccionario
                descripcion_estado = estados_dict.get(nuevo_estado, 'Estado desconocido')

                if citacion_numero == '':
                    asunto = "Trámite Permiso de Circulación N. Solicitud: {}".format(solicitud_atv)
                    cuerpo = "Estimado usuario su Trámite Permiso de Circulación ha cambiado de estado {}".format(descripcion_estado)
                    generar_pdf(request, tramite.id)
                elif citacion_numero is None:
                    asunto = "Trámite Permiso de Circulación N. Solicitud: {}".format(solicitud_atv)
                    cuerpo = "Estimado usuario su Trámite Permiso de Circulación ha cambiado de estado {}".format(descripcion_estado)
                    generar_pdf(request, tramite.id)
                else:
                    asunto = "Trámite Impugnación de Multas N. Citación: {}".format(citacion_numero)
                    cuerpo = "Estimado usuario su Trámite Impugnación de Multas ha cambiado de estado {}".format(descripcion_estado)
                
                send_mail(asunto,
                  cuerpo,
                  'luis.pilay@gizlocorp.com', [email])
                
                return JsonResponse({'success': True, 'message': 'Estado actualizado correctamente'})
            else:
                return JsonResponse({'success': False, 'message': 'No se encontró una multa relacionada con el número de citación proporcionado'}, status=404)
        except Tramites.DoesNotExist:
            return JsonResponse({'success': False, 'message': 'No se encontró un tramite con la solicitud proporcionada'}, status=404)
        except Exception as e:
            return JsonResponse({'success': False, 'message': str(e)}, status=500)
    else:
        return JsonResponse({'success': False, 'message': 'Método no permitido'}, status=405)

def sociosVehiculosIndex(request):
    usuario = request.user
    perfil = usuario_detalle.objects.select_related(
        'tipo_identificacion').get(usuario=usuario)

    usuarioAutentificado = {'identificacion': perfil.identificacion,
                            'apellidos': usuario.last_name,
                            'nombres': usuario.first_name}
    tramiteSociosVehiculos = Tramites.objects.filter(
        tipo_id=3).order_by('creacion')

    estados = [
        ('A', 'ACTIVO'),
        ('I', 'INACTIVO'),
        ('RS', 'REVISIÓN DE SOLICITUD'),
        ('PP', 'PENDIENTE DE PAGO'),
        ('P', 'PAGADO'),
        ('R', 'RECHAZADO'),
        ('AP', 'APROBADO'),
    ]

    # Agregar la fecha a la lista de tramites
    tramiteSociosVehiculos = [{'id': t.id, 'tipo_tramite_socio_vehiculo': t.tipo_tramite_socio_vehiculo, 'estado': t.estado, 'fecha': t.creacion} for t in tramiteSociosVehiculos]

    return render(request, 'socios_vehiculos/index.html',
                  {'usuarioAutentificado': usuarioAutentificado,
                   'tramiteSociosVehiculos': tramiteSociosVehiculos,
                   'estados': estados})


def socioVehiculosNuevo(request, tipo):
    usuario = request.user
    perfil = usuario_detalle.objects.select_related(
        'tipo_identificacion').get(usuario=usuario)

    usuarioAutentificado = {'identificacion': perfil.identificacion,
                            'apellidos': usuario.last_name,
                            'nombres': usuario.first_name}
    requisitosTramites = Requisitos_Tramites.objects.filter(
        tipo_id=3, estado='A', tipo_tramite_socio_vehiculo=tipo)
    numeroRequisitos = Requisitos_Tramites.objects.filter(
        tipo_id=3, estado='A', tipo_tramite_socio_vehiculo=tipo).count()
    return render(request, 'socios_vehiculos/nuevo.html',
                  {'usuarioAutentificado': usuarioAutentificado,
                   'requisitosTramites': requisitosTramites,
                   'numeroRequisitos': numeroRequisitos,
                   'tipoTramiteSocioVehiculo': tipo})


def crearTramiteSocioVehiculo(request):
    try:
        usuario = request.user
        tipoTramiteSocioVehiculo = request.POST.get('tipoTramiteSocioVehiculo')
        cabecera = request.POST.get('cabecera')
        tramite_obj = Tramites.objects.create(tipo_id=3,
                                          usuario=usuario.username,
                                          tipo_tramite_socio_vehiculo=tipoTramiteSocioVehiculo,
                                          cabecera_solicitud=cabecera)
        data = {
            'tramite_id': tramite_obj.id
        }
        print(tramite_obj.id)
        
        return JsonResponse(data)
    except Tramites.DoesNotExist:
        return JsonResponse({'error': 'Trámite no encontrado'}, status=404)

def subir_archivo(request):
    if request.method == 'POST':
        usuario = request.user
        form = RequisitoForm(request.POST, request.FILES)
        requisitoId = request.POST.get('requisitoId')
        tramiteId = request.POST.get('tramiteId')

        if form.is_valid():
            requisito = Tramite_socios_vehiculos.objects.create(tramite_id=tramiteId,
                                                                requisito=requisitoId,
                                                                archivos=form.cleaned_data['archivos'],
                                                                usuario=usuario.username)

            return JsonResponse({'success': True, 'message': 'Archivo subido correctamente', 'data': {
                'ruta': requisito.archivos.url
            }})
        else:
            return JsonResponse({'success': False, 'message': 'Error en el formulario'})
    return JsonResponse({'success': False, 'message': 'Método no permitido'})


def socioVehiculosSolicitar(request):
    if request.method == 'POST':
        data = request.POST
        tramiteId = request.POST.get('tramiteId')
        print(tramiteId)
        tramite = Tramites.objects.get(id=tramiteId)
        tramite.estado = 'RS'
        tramite.save()
        response_data = {
            'status': 'success',
            'message': 'Formulario creado correctamente.',
            'redirect_url':  reverse('socios_vehiculos')
        }
        return JsonResponse(response_data)

    return JsonResponse({'status': 'error', 'message': 'Método no permitido'}, status=405)

def header_footer(canvas, doc):
    # Header
    header_data = [['Encabezado 1', 'Encabezado 2', 'Encabezado 3']]
    header_table = Table(header_data, colWidths=[doc.width/3.0] * 3)
    header_table.setStyle(TableStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
        ('BACKGROUND', (0, 0), (-1, 0), colors.darkblue),
        ('GRID', (0, 0), (-1, -1), 1, colors.black),
    ]))
    
    width, height = A4
    header_table.wrapOn(canvas, doc.width, doc.topMargin)
    header_table.drawOn(canvas, doc.leftMargin, height - doc.topMargin + 10*mm)

    # Footer
    footer_data = [['Pie de página 1', 'Pie de página 2', 'Pie de página 3']]
    footer_table = Table(footer_data, colWidths=[doc.width/3.0] * 3)
    footer_table.setStyle(TableStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.grey),
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('TOPPADDING', (0, 0), (-1, 0), 12),
        ('BACKGROUND', (0, 0), (-1, 0), colors.darkblue),
        ('GRID', (0, 0), (-1, -1), 1, colors.black),
    ]))
    
    footer_table.wrapOn(canvas, doc.width, doc.bottomMargin)
    footer_table.drawOn(canvas, doc.leftMargin, doc.bottomMargin - 15*mm)

def generar_pdf(request, tramite_id):
    # Crear la carpeta personalizada si no existe
    custom_dir = os.path.join(settings.MEDIA_ROOT, 'permiso_circulacion')
    if not os.path.exists(custom_dir):
        os.makedirs(custom_dir)

    # Ruta donde se guardará el PDF
    pdf_path = os.path.join(custom_dir, 'permiso_circulacion_'+str(tramite_id)+'.pdf')

    # Crear el documento PDF
    doc = SimpleDocTemplate(pdf_path, pagesize=letter, rightMargin=0.75*inch, leftMargin=0.75*inch)

    styles = getSampleStyleSheet()    
    elements = []


    image1_path = os.path.join(settings.MEDIA_ROOT, 'static', 'img', 'logo_sambo.jpg')
    image2_path = os.path.join(settings.MEDIA_ROOT, 'static', 'img', 'IMAGEN-EMPRESA.jpg')    
    image1 = Image(image1_path, width=100, height=50)
    image2 = Image(image2_path, width=120, height=50)
    colWidths = [100,150, 130] 

    
    table_data = [[image1,'' ,image2],
                  ['','',  '']]  
    table = Table(table_data, colWidths=colWidths)  # Ajuste de los anchos de columna
    color_dorado = HexColor('#EFB810')  # Color azul claro
    color_gris = HexColor('#9B9B9B')
    table.setStyle(TableStyle([
        ('ALIGN', (0, 0), (0, 0), 'LEFT'),      # Alinear la primera imagen a la izquierda       
        ('ALIGN', (0, 2), (0, 2), 'RIGHT'),     # Alinear la segunda imagen a la derecha
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'), # Alinear verticalmente al centro        
        ('LINEBELOW', (0, 0), (0, 0), 2, color_dorado), 
        ('LINEBELOW', (1, 0), (1, 0), 2, color_gris), 
        ('LINEBELOW', (2, 0), (2, 0), 2, color_gris),         
    ]))
    left_aligned_style = ParagraphStyle(
        name='LeftAlignedStyle',
        parent=styles['Normal'],
        alignment=2 # Alineación a la izquierda
    )
    
    elements.append(table)
    elements.append(Spacer(1, 20))

    # Título
    elements.append(Paragraph("Samborondon, - 19 de Julio de 2024", left_aligned_style))
    elements.append(Spacer(1, 12))
    elements.append(Paragraph("Ing. Juan Emilio Kronfle Ramírez, MBA.", styles['Normal']))
    elements.append(Paragraph("Gerente General de la Autoridad de Tránsito y Vigilancia de Samborondón", styles['Normal']))
    elements.append(Spacer(1, 12))
    elements.append(Paragraph("Permiso de Circulación", styles['Normal']))
    elements.append(Spacer(1, 24))

    
    username = "ALEX"
    lastname = "ARMIJOS"
    identity_number = "0951987619"
    address = "Ciudad de Guayaquil"
    activity = "Transporte de pasajeros"
    plate_number = "GTL5178"

    body_text = (
        f"Se otorga el presente permiso de circulación al {username} {lastname}, con cédula de identidad número {identity_number}, domiciliado en {address}. "
        f"El vehículo autorizado corresponde a la actividad {activity}, la placa {plate_number}. "
        "Este permiso es válido desde el 05 de agosto de 2024 hasta el 04 de agosto de 2025. "
        "El vehículo ha cumplido con todos los requisitos técnicos y legales para circular, y el propietario se compromete a mantenerlo en condiciones óptimas de funcionamiento y seguridad. "
        "Este documento debe ser presentado a las autoridades competentes cuando sea requerido."
    )
    elements.append(Paragraph(body_text, styles['Normal']))
    elements.append(Spacer(1, 24))

    # Notas adicionales
    notes_text = (
        "Nota: Copias de los documentos que se deben acompañar en formato PDF.<br/>"       
    )
    elements.append(Paragraph(notes_text, styles['Normal']))

    # Construir el PDF y guardarlo en la ruta especificada
    doc.build(elements)

    return HttpResponse(f"PDF guardado en: {pdf_path}")

def descargar_pdf(request, tramite_id):
    pdf_path = os.path.join(settings.MEDIA_ROOT, 'permiso_circulacion', 'permiso_circulacion_{}.pdf'.format(tramite_id))
    return FileResponse(open(pdf_path, 'rb'), content_type='application/pdf')

def enviar_correo(request):
    print("Vista enviar_correo ejecutada")  # Agrega este print statement para verificar que se esté ejecutando la vista
    asunto = "prueba"
    cuerpocorreo = "prueba"
    correo = "luispilay27@hotmail.com"
    print(f'correo: {correo}')
    try:
        send_mail(asunto, cuerpocorreo, 'luis.pilay@gizlocorp.com', [correo])
        print("Correo electrónico enviado correctamente")  # Agrega este print statement para verificar que se esté enviando el correo electrónico
        messages.success(request, 'Correo enviado exitosamente.')
        return redirect('info_contact')
    except Exception as e:
        print(f"Ha ocurrido un error: {e}")  # Agrega este print statement para verificar que no haya errores
        messages.error(request, 'El correo electrónico no está registrado.')
    return render(request, 'info_contact.html')
