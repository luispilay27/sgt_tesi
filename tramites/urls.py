from django.urls import path, include
from tramites import views

urlpatterns = [
    path('permiso_circulacion/', views.permisoCirculacionIndex, name='permiso_circulacion'),
    path('permiso_circulacion/nuevo', views.permisoCirculacionNuevo, name='permiso_circulacion_nuevo'),
    path('permiso_circulacion/solicitar', views.permisoCirculacionSolicitar, name='permiso_circulacion_solicitar'),
    path('multas/', views.multas, name='multas'),
    path('multas/impugnaciones/', views.multasImpugnadas, name='multas_impugnaciones'),
    path('multas/consultar/<str:placa>/', views.multasConsultar, name='multas_consultar'),
    path('multas/impugnar/', views.multasImpugnar, name='multas_impugnar'),    
    path('socios-vehiculos', views.sociosVehiculosIndex, name='socios_vehiculos'),
    path('socios-vehiculos/nuevo/<str:tipo>/', views.socioVehiculosNuevo, name='socios_vehiculos_nuevo'),
    path('socios-vehiculos/tramite/crear', views.crearTramiteSocioVehiculo, name='crearTramiteSocioVehiculo'),
    path('socios-vehiculos/tramite/solicitar', views.socioVehiculosSolicitar, name='socios_vehiculos_solicitar'),
    path('subir-archivo/', views.subir_archivo, name='subir_archivo'),
    path('permiso_circulacion/generar-pdf/<str:tramite_id>/', views.generar_pdf, name='generar_pdf'),
    path('multas/tramite/actualizar-estado', views.actualizar_estado_tramite, name='multa_tramite_actualizar_estado'),
    path('enviar_correo', views.enviar_correo, name='enviar_correo'),
    path('descargar_pdf/<int:tramite_id>/', views.descargar_pdf, name='descargar_pdf'),
]
