# Generated by Django 5.0.4 on 2024-08-05 00:47

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('mantenimientos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Api_consumos_bitacora',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(max_length=255)),
                ('modulo', models.CharField(max_length=100)),
                ('request', models.JSONField()),
                ('response', models.JSONField()),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tipos_tramite',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.CharField(max_length=100)),
                ('estado', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo')], default='A', max_length=3)),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(blank=True, null=True)),
                ('usuario', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Tokens',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('token', models.CharField(max_length=255)),
                ('estado', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo')], default='A', max_length=1)),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(blank=True, null=True)),
                ('expira', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Requisitos_Tramites',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.TextField()),
                ('descripcion', models.TextField()),
                ('tipo_tramite_socio_vehiculo', models.CharField(blank=True, max_length=50, null=True)),
                ('estado', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo')], default='A', max_length=3)),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(blank=True, null=True)),
                ('usuario', models.CharField(blank=True, max_length=50, null=True)),
                ('tipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tramites.tipos_tramite')),
            ],
        ),
        migrations.CreateModel(
            name='Tramites',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('placa', models.CharField(blank=True, max_length=50, null=True)),
                ('solicitud_atv', models.CharField(blank=True, max_length=100, null=True)),
                ('codigo_atv', models.CharField(blank=True, max_length=100, null=True)),
                ('cabecera_solicitud', models.JSONField(blank=True, null=True)),
                ('tipo_tramite_socio_vehiculo', models.CharField(blank=True, max_length=50, null=True)),
                ('estado', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo'), ('RS', 'Revision de Solicitud'), ('PP', 'Pendiente de Pago'), ('P', 'Pagado'), ('R', 'Rechazado'), ('AP', 'Aprobado')], default='A', max_length=3)),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(blank=True, null=True)),
                ('usuario', models.CharField(max_length=50)),
                ('tipo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tramites.tipos_tramite')),
            ],
        ),
        migrations.CreateModel(
            name='Tramite_socios_vehiculos',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(max_length=20)),
                ('requisito', models.CharField(max_length=200)),
                ('archivos', models.FileField(blank=True, null=True, upload_to='requisitos/')),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(blank=True, null=True)),
                ('usuario', models.CharField(max_length=50)),
                ('tramite', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tramites.tramites')),
            ],
        ),
        migrations.CreateModel(
            name='Tramite_permiso_circulacion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identificacion', models.CharField(max_length=250)),
                ('tonelaje', models.CharField(max_length=250)),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(blank=True, null=True)),
                ('usuario', models.CharField(max_length=50)),
                ('actividad', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mantenimientos.actividades')),
                ('tramite', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tramites.tramites')),
            ],
        ),
        migrations.CreateModel(
            name='Tramite_multa_impugnar',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('citacion', models.CharField(max_length=50)),
                ('identificacion', models.CharField(max_length=15)),
                ('nombres', models.CharField(max_length=200)),
                ('telefono', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=100)),
                ('mensaje', models.CharField(max_length=500)),
                ('ruta_pdf', models.URLField()),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('actualizacion', models.DateTimeField(blank=True, null=True)),
                ('usuario', models.CharField(max_length=50)),
                ('tramite', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tramites.tramites')),
            ],
        ),
    ]
