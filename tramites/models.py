from django.db import models
from mantenimientos.models import Actividades

class Tokens(models.Model):
    OPCIONES_ESTADO = [                
        ('A', 'Activo'),
        ('I', 'Inactivo'),
    ]
    token = models.CharField(max_length=255)
    estado = models.CharField(
            max_length=1,
            choices=OPCIONES_ESTADO,
            default='A', 
        )
    creacion = models.DateTimeField(auto_now_add=True)
    actualizacion = models.DateTimeField(null=True, blank=True)
    expira = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.token
    
class Api_consumos_bitacora(models.Model):    
    url = models.CharField(max_length=255)
    modulo = models.CharField(max_length=100)
    request = models.JSONField()
    response = models.JSONField()
    creacion = models.DateTimeField(auto_now_add=True)
    actualizacion = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.token
    
class Tipos_tramite(models.Model):
    OPCIONES_ESTADO = [                
        ('A' , 'Activo'),
        ('I' , 'Inactivo')
    ]
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=100)
    estado = models.CharField(
            max_length=3,
            choices=OPCIONES_ESTADO,
            default='A', 
        )
    creacion = models.DateTimeField(auto_now_add=True)
    actualizacion = models.DateTimeField(null=True, blank=True)
    usuario = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre   
class Tramites(models.Model):
    OPCIONES_ESTADO = [                
        ('A' , 'Activo'),
        ('I' , 'Inactivo'),
        ('RS' ,'Revision de Solicitud'),        
        ('PP' , 'Pendiente de Pago'),
        ('P' , 'Pagado'),
        ('R' , 'Rechazado'),
        ('AP', 'Aprobado'),
    ]

    tipo_tramite_socio_vehiculo = [                
        ('VINCULACIÓN'),
        ('DESVINCULACIÓN')
    ]
    id = models.AutoField(primary_key=True)
    tipo = models.ForeignKey(Tipos_tramite, on_delete=models.CASCADE)
    placa = models.CharField(max_length=50,null=True, blank=True)
    solicitud_atv = models.CharField(max_length=100,null=True, blank=True)
    codigo_atv = models.CharField(max_length=100,null=True, blank=True)
    cabecera_solicitud =  models.JSONField(null=True, blank=True) 
    tipo_tramite_socio_vehiculo = models.CharField(max_length=50,null=True, blank=True)  
    estado = models.CharField(
            max_length=3,
            choices=OPCIONES_ESTADO,
            default='A', 
        )        
    creacion = models.DateTimeField(auto_now_add=True)    
    actualizacion = models.DateTimeField(null=True, blank=True)
    usuario = models.CharField(max_length=50)

    def __str__(self):
        return self.solicitud_atv 
class Tramite_multa_impugnar(models.Model):    
    tramite = models.ForeignKey(Tramites, on_delete=models.CASCADE)
    citacion = models.CharField(max_length=50)
    identificacion = models.CharField(max_length=15)
    nombres = models.CharField(max_length=200)
    telefono = models.CharField(max_length=50)
    email = models.CharField(max_length=100)
    mensaje = models.CharField(max_length=500)
    ruta_pdf = models.URLField()
    creacion = models.DateTimeField(auto_now_add=True)    
    actualizacion = models.DateTimeField(null=True, blank=True)
    usuario = models.CharField(max_length=50)

    def __str__(self):
        return self.tramite
class Tramite_permiso_circulacion(models.Model):
    tramite = models.ForeignKey(Tramites, on_delete=models.CASCADE)
    identificacion =  models.CharField(max_length=250)
    tonelaje =  models.CharField(max_length=250)
    actividad =  models.ForeignKey(Actividades, on_delete=models.CASCADE) 
    pdf_ruta = models.FileField(upload_to='permiso_circulacion/', null=True, blank=True)   
    creacion = models.DateTimeField(auto_now_add=True)    
    actualizacion = models.DateTimeField(null=True, blank=True)
    usuario = models.CharField(max_length=50)
    
    def __str__(self):
        return self.tramite
    
class Tramite_socios_vehiculos(models.Model):        
    tramite = models.ForeignKey(Tramites, on_delete=models.CASCADE) 
    tipo =  models.CharField(max_length=20)        
    requisito = models.CharField(max_length=200)      
    archivos = models.FileField(upload_to='requisitos/', null=True, blank=True)    
    creacion = models.DateTimeField(auto_now_add=True)    
    actualizacion = models.DateTimeField(null=True, blank=True)
    usuario = models.CharField(max_length=50)
    
    def __str__(self):
        return self.tramite

class Requisitos_Tramites(models.Model):
    OPCIONES_ESTADO = [                
        ('A' , 'Activo'),
        ('I' , 'Inactivo'),
    ]
    
    tipo = models.ForeignKey(Tipos_tramite, on_delete=models.CASCADE)    
    nombre = models.TextField() 
    descripcion = models.TextField() 
    tipo_tramite_socio_vehiculo = models.CharField(max_length=50,null=True, blank=True)  
    estado = models.CharField(
            max_length=3,
            choices=OPCIONES_ESTADO,
            default='A', 
        )        
    creacion = models.DateTimeField(auto_now_add=True)    
    actualizacion = models.DateTimeField(null=True, blank=True)
    usuario = models.CharField(max_length=50,null=True, blank=True)